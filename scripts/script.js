"use strict";
// 1. Метод forEach - це метод, який виконує функцію один раз для кожного елемента в массиві. 
// 2. Масив можна очистити, наприклад, якщо присвоїти новий але пустий масив. Також можна використати властивіть довжини. 
// 3. Метод Array. isArray(). 

let arr = ['hello', 'world', 23, '23', null];

function filterBy(arr, arg) {
    let filtered = arr.filter(elem => typeof elem !== arg);
    console.log(filtered);
}
filterBy(arr, "string");
